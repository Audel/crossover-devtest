package com.audeldugarte.crossoverdevtest;

import com.audeldugarte.crossoverdevtest.utils.UtilFunctions;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by audel on 9/7/16.
 */
public class UtilFunctionsTest {

    @Test
    public void testCalculateMd5(){
        String[] actual = {UtilFunctions.calculateMd5("12345"),
                UtilFunctions.calculateMd5("abc123"),
                UtilFunctions.calculateMd5("abc**123"),
                UtilFunctions.calculateMd5("p4ssw0rd"),
                UtilFunctions.calculateMd5("t3st*-p4ss")};

        String[] expected = {"827CCB0EEA8A706C4C34A16891F84E7B".toLowerCase(),
        "E99A18C428CB38D5F260853678922E03".toLowerCase(),
        "8EDF439406910FBCB79DD3A832D4AF2D".toLowerCase(),
        "2A9D119DF47FF993B662A8EF36F9EA20".toLowerCase(),
        "0414561F6E7371DE51DD43EE521EFE79".toLowerCase()};

        for(int i =0; i<expected.length; i++){
            assertEquals("testing MD5 generation", actual[i], expected[i]);
        }
    }

    @Test
    public void testValidateEmail(){
        Boolean[] actual = {UtilFunctions.checkValidEmail("doctor@confs.com"),
        UtilFunctions.checkValidEmail("admin@mail.com"),
        UtilFunctions.checkValidEmail("mail.com"),
        UtilFunctions.checkValidEmail("email@"),
        UtilFunctions.checkValidEmail("email@.com")};

        Boolean[] expected = {true, true, false, false, false};

        for(int i =0; i<expected.length; i++){
            assertEquals("testing email validation", actual[i], expected[i]);
        }
    }


}
