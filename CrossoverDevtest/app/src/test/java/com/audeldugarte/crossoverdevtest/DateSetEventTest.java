package com.audeldugarte.crossoverdevtest;

import org.junit.Test;

import static org.junit.Assert.*;

import com.audeldugarte.crossoverdevtest.internalcomms.DatePickedEvent;

/**
 * Created by audel on 9/7/16.
 */
public class DateSetEventTest {

    @Test
    public void testDateStringCreation(){
        DatePickedEvent actualDate =  new DatePickedEvent(12,10,2016);
        String actualStr = actualDate.getStringDate();
        String expected = "2016-10-12";

        assertEquals("testing Date String creation", actualStr, expected);
    }
}
