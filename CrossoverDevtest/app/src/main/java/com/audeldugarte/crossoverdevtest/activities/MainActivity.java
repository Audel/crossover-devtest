package com.audeldugarte.crossoverdevtest.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import com.audeldugarte.crossoverdevtest.R;
import com.audeldugarte.crossoverdevtest.models.User;
import com.audeldugarte.crossoverdevtest.utils.UtilFunctions;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    @Override
    protected void onResume() {
        super.onResume();

        Log.d("MainActivity", "resuming main activity and checking");

        if(User.listAll(User.class).size() < 1){
            User firstAdmin = new User("Admin", "admin@medsconf.com", UtilFunctions.calculateMd5("1234"), 2l);
            firstAdmin.save();
        }

        Log.d("MainActivity", "starting next Activity");
        Intent i = new Intent(getApplicationContext(), LoginActivity.class);
        startActivity(i);
    }
}
