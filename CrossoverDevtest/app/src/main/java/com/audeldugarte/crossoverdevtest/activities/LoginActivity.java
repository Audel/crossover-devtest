package com.audeldugarte.crossoverdevtest.activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.audeldugarte.crossoverdevtest.R;
import com.audeldugarte.crossoverdevtest.fragments.RegisterDialogFragment;
import com.audeldugarte.crossoverdevtest.models.User;
import com.audeldugarte.crossoverdevtest.utils.UtilFunctions;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class LoginActivity extends AppCompatActivity {

    private final String TAG = "LoginActivity";

    private EditText userEmail;
    private EditText userPassword;
    private Button buttonGoLogin;
    private TextView textRegister;
    private TextView textRemeberPass;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        initViews();
    }

    private void initViews(){

        userEmail = (EditText) findViewById(R.id.editTextLoginEmail);
        userPassword = (EditText) findViewById(R.id.editTextLoginPassword);
        buttonGoLogin = (Button) findViewById(R.id.buttonDoLogin);
        textRegister = (TextView) findViewById(R.id.textGoRegisterUser);

        buttonGoLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!userEmail.getText().toString().isEmpty() && !userPassword.getText().toString().isEmpty()){

                    if(UtilFunctions.checkValidEmail(userEmail.getText().toString())){
                        sendLoginData();
                    }else{
                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.invalid_email), Toast.LENGTH_LONG).show();
                    }
                }
            }
        });

        textRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager fm = getSupportFragmentManager();
                RegisterDialogFragment regDialog = new RegisterDialogFragment();
                regDialog.show(fm, "fragment_register_user");
            }
        });

    }

    private void goNextActivity(){
        /*Intent i = new Intent(getApplicationContext(), HomeReservationsActivity.class);
        startActivity(i);*/
        Intent i = new Intent(getApplicationContext(), MenuActivity.class);
        startActivity(i);
    }

    private void sendLoginData(){
        List<User> possibleMatches = User.find(User.class, "email = ?", userEmail.getText().toString());
        if(possibleMatches.size()<1){
            Toast.makeText(getApplicationContext(), getResources().getString(R.string.wrong_login), Toast.LENGTH_LONG).show();
        }else{
            if(possibleMatches.get(0).getPassword().equals(UtilFunctions.calculateMd5(userPassword.getText().toString()))){
                Log.d(TAG, "user logged successfully");

                SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                SharedPreferences.Editor editor = settings.edit();
                editor.putString("username", possibleMatches.get(0).getName());
                editor.putString("useremail", possibleMatches.get(0).getEmail());
                editor.putLong("usertype", possibleMatches.get(0).getUserType());
                editor.putLong("userid", possibleMatches.get(0).getId());
                editor.commit();

                Intent i = new Intent(getApplicationContext(), MenuActivity.class);
                startActivity(i);

            }
        }
    }

    @Override
    public void onBackPressed() {
        //does noting
        //super.onBackPressed();
    }
}
