package com.audeldugarte.crossoverdevtest.models;

import com.orm.SugarRecord;

/**
 * Created by audel on 9/4/16.
 */
public class ConfInvitation extends SugarRecord{

    private String invDate;
    private String invitedEmail;
    private Long invitedId;
    private String inviterEmail;
    private String inviterName;
    private String confName;
    private Long confId;

    public ConfInvitation() {}

    public ConfInvitation(String invDate, String invitedEmail, Long invitedId,
                          String inviterEmail, String inviterName, Long confId, String confName ) {
        this.invDate = invDate;
        this.invitedEmail = invitedEmail;
        this.invitedId = invitedId;
        this.inviterEmail = inviterEmail;
        this.inviterName = inviterName;
        this.confId = confId;
        this.confName = confName;
    }

    public String getInvDate() {
        return invDate;
    }

    public void setInvDate(String invDate) {
        this.invDate = invDate;
    }

    public String getInvitedEmail() {
        return invitedEmail;
    }

    public void setInvitedEmail(String invitedEmail) {
        this.invitedEmail = invitedEmail;
    }

    public Long getInvitedId() {
        return invitedId;
    }

    public void setInvitedId(Long invitedId) {
        this.invitedId = invitedId;
    }

    public String getInviterEmail() {
        return inviterEmail;
    }

    public void setInviterEmail(String inviterEmail) {
        this.inviterEmail = inviterEmail;
    }

    public String getInviterName() {
        return inviterName;
    }

    public void setInviterName(String inviterName) {
        this.inviterName = inviterName;
    }

    public Long getConfId() {
        return confId;
    }

    public void setConfId(Long confId) {
        this.confId = confId;
    }

    public String getConfName() {
        return confName;
    }

    public void setConfName(String confName) {
        this.confName = confName;
    }
}
