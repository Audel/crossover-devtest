package com.audeldugarte.crossoverdevtest.activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.audeldugarte.crossoverdevtest.R;
import com.audeldugarte.crossoverdevtest.fragments.InviteDoctorDialogFragment;
import com.audeldugarte.crossoverdevtest.models.ConferenceTopic;
import com.audeldugarte.crossoverdevtest.models.MedConference;

import java.util.List;

public class MedConfDetailActivity extends AppCompatActivity {

    private TextView textConfDetailName;
    private TextView textConfDetailLocation;
    private TextView textConfDetailTopic;
    private TextView textConfDetailStartDate;
    private TextView textConfDetailEndDate;
    private Button btnInviteDoctorToConf;
    private Button btnDeleteThisConf;
    private Button btnEditThisConf;

    private MedConference actualCOnference;

    private Long confId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_med_conf_detail);

        confId = getIntent().getExtras().getLong("conference_id");

        initViews();
    }

    private void initViews(){
        textConfDetailName = (TextView) findViewById(R.id.textMedConfDetailName);
        textConfDetailLocation = (TextView) findViewById(R.id.textMedConfDetailLocation);
        textConfDetailTopic = (TextView) findViewById(R.id.textMedConfDetailTopic);
        textConfDetailStartDate = (TextView) findViewById(R.id.textMedConfDetailStartDate);
        textConfDetailEndDate = (TextView) findViewById(R.id.textMedConfDetailEndDate);

        btnInviteDoctorToConf = (Button) findViewById(R.id.buttonInviteDoctorToConference);
        btnDeleteThisConf = (Button) findViewById(R.id.buttonDeleteThisConference);
        btnEditThisConf = (Button) findViewById(R.id.buttonEditThisConference);

        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        Long loggegType = settings.getLong("usertype", 0);
        if(loggegType == 1){
            ((RelativeLayout) findViewById(R.id.layoutConferenceDetailForAdmin)).setVisibility(View.GONE);
        }

        actualCOnference = MedConference.findById(MedConference.class, confId);

        textConfDetailName.setText(actualCOnference.getName());
        textConfDetailLocation.setText(actualCOnference.getLocation());
        textConfDetailStartDate.setText(actualCOnference.getStartDate());
        textConfDetailEndDate.setText(actualCOnference.getEndDate());

        ConferenceTopic actualTopic = ConferenceTopic.findById(ConferenceTopic.class, actualCOnference.getConferenceTopic());
        textConfDetailTopic.setText(actualTopic.getName());

        btnDeleteThisConf.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                actualCOnference.delete();
                finish();
            }
        });

        btnEditThisConf.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getApplicationContext(), CreateMedConfActivity.class);
                Bundle extras = new Bundle();
                extras.putLong("id_conf_edit", confId);
                i.putExtras(extras);
                startActivity(i);
            }
        });

        btnInviteDoctorToConf.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentManager fm = getSupportFragmentManager();
                InviteDoctorDialogFragment regDialog = new InviteDoctorDialogFragment();
                Bundle extras = new Bundle();
                extras.putLong("conference_id", confId);
                regDialog.setArguments(extras);
                regDialog.show(fm, "fragment_invite_user");
            }
        });

    }

    private void createAlertDialog(String message, Long confClickedId){
        final Long idConfFinal = confClickedId;

        AlertDialog.Builder builder = new AlertDialog.Builder(getApplicationContext());
        // Add the buttons
        builder.setPositiveButton(R.string.label_edit, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                // User clicked Edit button

            }
        });
        builder.setNeutralButton(R.string.label_delete, new DialogInterface.OnClickListener(){
            public void onClick(DialogInterface dialog, int id) {
                // Uses neutral buton to Delete the conference
                MedConference conDel = MedConference.findById(MedConference.class, idConfFinal);
                conDel.delete();
            }
        });
        builder.setNegativeButton(R.string.label_cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                // User cancelled the dialog
            }
        });
        // Set other dialog properties
        builder.setTitle("Suggested Topic").setMessage(message);

        // Create the AlertDialog
        AlertDialog dialog = builder.create();
    }
}
