package com.audeldugarte.crossoverdevtest.models;

import com.orm.SugarRecord;

/**
 * Created by audel on 9/4/16.
 */
public class User extends SugarRecord{

    private String name;
    private String email;
    private String password;
    private Long userType;

    public User() {}

    public User(String name, String email, String password, Long userType) {
        this.name = name;
        this.email = email;
        this.password = password;
        this.userType = userType;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Long getUserType() {
        return userType;
    }

    public void setUserType(Long userType) {
        this.userType = userType;
    }
}
