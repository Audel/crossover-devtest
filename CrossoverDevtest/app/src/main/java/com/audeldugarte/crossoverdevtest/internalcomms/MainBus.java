package com.audeldugarte.crossoverdevtest.internalcomms;

/**
 * Created by audel on 9/4/16.
 */
public class MainBus extends RxEventBus {
    private static MainBus instance;

    public static MainBus getInstance() {
        if (instance == null)
            instance = new MainBus();
        return instance;
    }

    private MainBus() {
    }
}
