package com.audeldugarte.crossoverdevtest.internalcomms;

/**
 * Created by audel on 9/4/16.
 */
public class ConfClickRxEvent {

    private String name;
    private Long confId;

    public ConfClickRxEvent() {}

    public ConfClickRxEvent(String name, Long confId) {
        this.name = name;
        this.confId = confId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getConfId() {
        return confId;
    }

    public void setConfId(Long confId) {
        this.confId = confId;
    }
}
