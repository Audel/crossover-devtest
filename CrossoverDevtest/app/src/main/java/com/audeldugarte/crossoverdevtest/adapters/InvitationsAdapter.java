package com.audeldugarte.crossoverdevtest.adapters;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.provider.CalendarContract;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.audeldugarte.crossoverdevtest.R;
import com.audeldugarte.crossoverdevtest.internalcomms.ConfClickRxEvent;
import com.audeldugarte.crossoverdevtest.internalcomms.MainBus;
import com.audeldugarte.crossoverdevtest.models.ConfInvitation;
import com.audeldugarte.crossoverdevtest.models.MedConference;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

/**
 * Created by audel on 9/5/16.
 */
public class InvitationsAdapter extends RecyclerView.Adapter<InvitationsAdapter.ViewHolder>{

    private Context context;
    private List<ConfInvitation> dataset;

    public InvitationsAdapter(Context context, List<ConfInvitation> dataset){
        this.context = context;
        this.dataset = dataset;
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        private TextView confName;
        private TextView confDate;
        private TextView confType;
        private TextView inviterName;
        private Button btnAccept;
        private Button btnIgnore;

        public ViewHolder(View itemView) {
            super(itemView);

            confName = (TextView) itemView.findViewById(R.id.textInvitationItemConfName);
            confDate = (TextView) itemView.findViewById(R.id.textInvitationItemConfType);
            confType = (TextView) itemView.findViewById(R.id.textInvitationItemConfDate);
            inviterName = (TextView) itemView.findViewById(R.id.textInvitationItemInviterName);
            btnAccept = (Button) itemView.findViewById(R.id.buttonAcceptInvitation);
            btnIgnore = (Button) itemView.findViewById(R.id.buttonIgnoreInvitation);
        }

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_invitation, parent, false);

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        //TODO make the stuff happen here

        holder.confName.setText(dataset.get(position).getConfName());
        holder.confDate.setText(dataset.get(position).getInvDate() );
        holder.confType.setText(dataset.get(position).getInviterEmail());
        holder.confName.setText(dataset.get(position).getInviterName());

        /*holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("FragmentDialog", "about to send the event to list activity");
                MainBus.getInstance().post(new ConfClickRxEvent(dataset.get(position).getInvitedEmail(),
                        dataset.get(position).getConfId()  ));
            }
        });*/

        holder.btnAccept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //TODO accept invitation and delete from list and DB
                ConfInvitation mInv = ConfInvitation.findById(ConfInvitation.class, dataset.get(position).getId());
                addAConfToCalendarPhone(mInv);
                mInv.delete();
                Log.d("InvitationsAdapter", "about to send the accept event to list activity");
                MainBus.getInstance().post(new ConfClickRxEvent(dataset.get(position).getInvitedEmail(),
                        dataset.get(position).getConfId()  ));
            }
        });

        holder.btnIgnore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ConfInvitation mInv = ConfInvitation.findById(ConfInvitation.class, dataset.get(position).getId());
                mInv.delete();
                Log.d("InvitationsAdapter", "about to send the ignore event to list activity");
                MainBus.getInstance().post(new ConfClickRxEvent(dataset.get(position).getInvitedEmail(),
                        dataset.get(position).getConfId()  ));
            }
        });
    }

    @Override
    public int getItemCount() {
        return dataset.size();
    }

    //************************************
    //*****ADDED SO WE CAN ADD TO CALENDAR
    //************************************

    private void addAConfToCalendarPhone(ConfInvitation mInvitation){
        if(mInvitation!=null){
            Calendar cal = Calendar.getInstance();
            Calendar calEnd = Calendar.getInstance();
            SimpleDateFormat format_api = new SimpleDateFormat("yyyy-MM-dd HH:mm");
            MedConference theConf = MedConference.findById(MedConference.class, mInvitation.getConfId());
            String fullDate = theConf.getStartDate() + " 08:00";
            String fullEndDate = theConf.getEndDate() + " 16:00";
            try {
                cal.setTime(format_api.parse(fullDate));
                calEnd.setTime(format_api.parse(fullEndDate));
            } catch (ParseException e) {
                e.printStackTrace();
            }
            if (Build.VERSION.SDK_INT >= 14) {
                Intent intent = new Intent(Intent.ACTION_INSERT)
                        .setData(CalendarContract.Events.CONTENT_URI)
                        .putExtra(CalendarContract.EXTRA_EVENT_BEGIN_TIME, cal.getTimeInMillis())
                        .putExtra(CalendarContract.EXTRA_EVENT_END_TIME, calEnd.getTimeInMillis())
                        .putExtra(CalendarContract.Events.TITLE, theConf.getName())
                        .putExtra(CalendarContract.Events.DESCRIPTION, theConf.getConferenceTopic())
                        .putExtra(CalendarContract.Events.EVENT_LOCATION, theConf.getLocation());
                //.putExtra(Events.AVAILABILITY, Events.AVAILABILITY_BUSY)
                //.putExtra(Intent.EXTRA_EMAIL, "rowan@example.com,trevor@example.com")
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);
            }else{
                Intent intent = new Intent(Intent.ACTION_EDIT);
                intent.setType("vnd.android.cursor.item/event");
                intent.putExtra("beginTime", cal.getTimeInMillis());
                //intent.putExtra("allDay", true);
                //intent.putExtra("rrule", "FREQ=YEARLY");
                intent.putExtra("endTime", calEnd.getTimeInMillis());
                intent.putExtra("title", theConf.getName());
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);
            }

        }
    }
}
