package com.audeldugarte.crossoverdevtest.internalcomms;

/**
 * Created by Audel on 9/5/2016.
 */
public class DatePickedEvent {
    private int day;
    private int month;
    private int year;

    public DatePickedEvent() {}

    public DatePickedEvent(int day, int month, int year) {
        this.day = day;
        this.month = month;
        this.year = year;
    }

    public String getStringDate(){
        return Integer.toString(this.year).concat("-").concat(Integer.toString(this.month)).
                concat("-").concat(Integer.toString(this.day));
    }

    public int getDay() {
        return day;
    }

    public void setDay(int day) {
        this.day = day;
    }

    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }
}
