package com.audeldugarte.crossoverdevtest.activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.RelativeLayout;

import com.audeldugarte.crossoverdevtest.R;
import com.audeldugarte.crossoverdevtest.models.SuggestedTopic;

public class MenuActivity extends AppCompatActivity {

    RelativeLayout layoutItemViewConfs;
    RelativeLayout layoutItemViewTopics;
    RelativeLayout layoutItemSuggestTopics;
    RelativeLayout layoutItemMyInvitations;
    RelativeLayout layoutItemCreateTopic;
    RelativeLayout layoutItemCreateConference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);

        initViews();
    }

    private void initViews(){
        layoutItemViewConfs = (RelativeLayout) findViewById(R.id.relativeLayoutMenuItemViewConfs);
        layoutItemViewTopics = (RelativeLayout) findViewById(R.id.relativeLayoutMenuItemViewTopics);
        layoutItemSuggestTopics = (RelativeLayout) findViewById(R.id.relativeLayoutMenuItemSuggestTopic);
        layoutItemMyInvitations = (RelativeLayout) findViewById(R.id.relativeLayoutMenuItemInvitations);
        layoutItemCreateTopic = (RelativeLayout) findViewById(R.id.relativeLayoutMenuItemCreateTopics);
        layoutItemCreateConference = (RelativeLayout) findViewById(R.id.relativeLayoutMenuCreateConferences);

        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        Long loggeduserType = settings.getLong("usertype", 0);

        if(loggeduserType == 1){
            layoutItemCreateTopic.setVisibility(View.GONE);
            layoutItemCreateConference.setVisibility(View.GONE);
        }else{
            layoutItemSuggestTopics.setVisibility(View.GONE);
            layoutItemMyInvitations.setVisibility(View.GONE);
        }

        layoutItemViewConfs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getApplicationContext(), ConferencesListActivity.class);
                startActivity(i);
            }
        });
        layoutItemCreateTopic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getApplicationContext(), TopicsViewActivity.class);
                startActivity(i);
            }
        });
        layoutItemViewTopics.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getApplicationContext(), SuggestedTopicsListActivity.class);
                startActivity(i);
            }
        });
        layoutItemCreateConference.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i =  new Intent(getApplicationContext(), CreateMedConfActivity.class);
                startActivity(i);
            }
        });
        layoutItemMyInvitations.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i =  new Intent(getApplicationContext(), ViewInvitationsActivity.class);
                startActivity(i);
            }
        });
        layoutItemSuggestTopics.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getApplicationContext(), SuggestNewTopicActivity.class);
                startActivity(i);
            }
        });


    }

    @Override
    public void onBackPressed() {
        //lets do nothing on back presed
        //super.onBackPressed();
    }
}
