package com.audeldugarte.crossoverdevtest.activities;

import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import com.audeldugarte.crossoverdevtest.R;
import com.audeldugarte.crossoverdevtest.adapters.MedConferenceAdapter;
import com.audeldugarte.crossoverdevtest.adapters.SuggestedTopicsAdapter;
import com.audeldugarte.crossoverdevtest.internalcomms.ConfClickRxEvent;
import com.audeldugarte.crossoverdevtest.internalcomms.MainBus;
import com.audeldugarte.crossoverdevtest.internalcomms.TopicClickEvent;
import com.audeldugarte.crossoverdevtest.models.ConferenceTopic;
import com.audeldugarte.crossoverdevtest.models.MedConference;
import com.audeldugarte.crossoverdevtest.models.SuggestedTopic;

import java.util.List;

import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class SuggestedTopicsListActivity extends AppCompatActivity {

    final static String TAG = "SuggestedTopicsActivity";

    private RecyclerView recyvlerViewSuggestedTopicsList;
    private List<SuggestedTopic> listSuggestedTopics;
    private Long loggeduserType;
    private SuggestedTopic touchedTopic;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_suggested_topics_list);

        initViews();
    }

    private void initViews(){

        recyvlerViewSuggestedTopicsList = (RecyclerView) findViewById(R.id.recyclerViewSuggestedTopics);

        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        loggeduserType = settings.getLong("usertype", 0);

        touchedTopic = new SuggestedTopic();

        loadDataToRecyclerView();
    }

    private void loadDataToRecyclerView(){
        listSuggestedTopics = SuggestedTopic.listAll(SuggestedTopic.class);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyvlerViewSuggestedTopicsList.setLayoutManager(mLayoutManager);

        SuggestedTopicsAdapter mAdapter = new SuggestedTopicsAdapter(getApplicationContext(), listSuggestedTopics);
        recyvlerViewSuggestedTopicsList.setAdapter(mAdapter);
    }

    private void createAlertDialog(String message){
        AlertDialog.Builder builder = new AlertDialog.Builder(getApplicationContext());
        // Add the buttons
        builder.setPositiveButton(R.string.label_accept, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                // User clicked OK button
                ConferenceTopic mTop =  new ConferenceTopic(touchedTopic.getName(), touchedTopic.getDescription());
                mTop.save();
                //and delete the suggested topic
                touchedTopic.delete();
                //and finally reload the recyclerview
                loadDataToRecyclerView();
            }
        });
        builder.setNegativeButton(R.string.label_cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                // User cancelled the dialog
            }
        });
        // Set other dialog properties
        builder.setTitle("Suggested Topic").setMessage(message);

        // Create the AlertDialog
        AlertDialog dialog = builder.create();
    }

    /************************************
     *****EVENT BUS W/RX JAVA CODE********
     *************************************/

    @Override
    protected void onResume() {
        super.onResume();
        // Subscribe to EventBus
        MainBus.getInstance().getBusObservable()
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(mainBusSubscriber);

    }

    private Subscriber<? super Object> mainBusSubscriber = new Subscriber<Object>() {
        @Override
        public void onCompleted() {
        }

        @Override
        public void onError(Throwable e) {
        }

        @Override
        public void onNext(Object o) {
            if (o instanceof TopicClickEvent) {
                TopicClickEvent event = (TopicClickEvent) o;
                Log.d(TAG, "received click from " + event.getSugTopic().getName());
                if(loggeduserType == 2){
                    createAlertDialog("want to accept this suggestion?");
                    touchedTopic.setName(event.getSugTopic().getName());
                    touchedTopic.setDescription(event.getSugTopic().getDescription());
                    touchedTopic.setId(event.getSugTopic().getId());
                }
            }
        }
    };

    @Override
    protected void onPause() {
        super.onPause();
        // Unsubscribe from EventBus
        mainBusSubscriber.unsubscribe();
    }
}
