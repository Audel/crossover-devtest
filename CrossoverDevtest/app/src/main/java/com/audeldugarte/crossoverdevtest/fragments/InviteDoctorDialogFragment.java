package com.audeldugarte.crossoverdevtest.fragments;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.audeldugarte.crossoverdevtest.R;
import com.audeldugarte.crossoverdevtest.models.ConfInvitation;
import com.audeldugarte.crossoverdevtest.models.MedConference;
import com.audeldugarte.crossoverdevtest.models.User;
import com.audeldugarte.crossoverdevtest.utils.UtilFunctions;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by audel on 9/5/16.
 */
public class InviteDoctorDialogFragment extends DialogFragment {

    private EditText textDoctorEmail;
    private Button buttonSendInvitation;
    private Button buttoncancelInvitation;
    private Long confId;

    public InviteDoctorDialogFragment() {
        // Empty constructor required for DialogFragment
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_dialog_invite_doctor, container);

        getDialog().setTitle("Invite a Doctor to this Conference");
        getDialog().setCanceledOnTouchOutside(false);

        confId = getArguments().getLong("conference_id");

        initViews(view);

        return view;
    }

    private void initViews(View mainView){
        textDoctorEmail = (EditText) mainView.findViewById(R.id.editTextEmailDoctorToInvire);
        buttonSendInvitation = (Button) mainView.findViewById(R.id.buttonDialogInviteDocotr);
        buttoncancelInvitation = (Button) mainView.findViewById(R.id.buttonDialogCancelInvitation);

        buttonSendInvitation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(UtilFunctions.checkValidEmail(textDoctorEmail.getText().toString()) ){

                        registerUser();

                }else{
                    Toast.makeText(getActivity(), getResources().getString(R.string.invalid_email), Toast.LENGTH_LONG).show();
                }

                /*Log.d("FragmentDialog", "about to send the event i think");
                MainBus.getInstance().post(new MiCommEvent("probando evento de prueba en Rx"));
                dismiss();*/
            }
        });

        buttoncancelInvitation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

    }

    private void registerUser(){

        Calendar mCal = Calendar.getInstance();
        SimpleDateFormat mFormat = new SimpleDateFormat("yyyy-MM-dd");

        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(getActivity());
        String loggedUsrEmail = settings.getString("useremail", "");
        String loggedUsrName = settings.getString("username", "");

        List<User> possibleMatches = User.find(User.class, "email = ?", textDoctorEmail.getText().toString());
        if(possibleMatches.size()<1){
            Toast.makeText(getActivity(), getResources().getString(R.string.user_inexistant), Toast.LENGTH_LONG).show();
        }else{
            MedConference invConf = MedConference.findById(MedConference.class, confId);

            Log.d("Debug_inviting", "inviting " + possibleMatches.get(0).getId());

            ConfInvitation mInvitaton = new ConfInvitation(mFormat.format(mCal.getTime()), textDoctorEmail.getText().toString(),
                    possibleMatches.get(0).getId(), loggedUsrEmail, loggedUsrName, confId, invConf.getName());

            mInvitaton.save();

            Toast.makeText(getActivity(), getResources().getString(R.string.invitation_success), Toast.LENGTH_LONG).show();

        }

        dismiss();
    }
}
