package com.audeldugarte.crossoverdevtest.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.audeldugarte.crossoverdevtest.R;

import com.audeldugarte.crossoverdevtest.internalcomms.ConfClickRxEvent;
import com.audeldugarte.crossoverdevtest.internalcomms.MainBus;
import com.audeldugarte.crossoverdevtest.models.ConferenceTopic;
import com.audeldugarte.crossoverdevtest.models.MedConference;

import org.w3c.dom.Text;

import java.util.List;

/**
 * Created by audel on 9/4/16.
 */
public class MedConferenceAdapter extends RecyclerView.Adapter<MedConferenceAdapter.ViewHolder> {

    private Context context;
    private List<MedConference> dataset;

    public MedConferenceAdapter(Context context, List<MedConference> dataset){
        this.context = context;
        this.dataset = dataset;
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        private TextView confName;
        private TextView confDate;
        private TextView confType;

        public ViewHolder(View itemView) {
            super(itemView);

            confName = (TextView) itemView.findViewById(R.id.textViewConfName);
            confDate = (TextView) itemView.findViewById(R.id.textViewConfDate);
            confType = (TextView) itemView.findViewById(R.id.textViewConfType);
        }

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_medconference, parent, false);

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        //TODO make the stuff happen here

        holder.confName.setText(dataset.get(position).getName());
        holder.confDate.setText(dataset.get(position).getStartDate());
        //TODO find the topic
        Long topicId = dataset.get(position).getConferenceTopic();
        ConferenceTopic mTopic = ConferenceTopic.findById(ConferenceTopic.class, topicId);
        holder.confType.setText(mTopic.getName());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("FragmentDialog", "about to send the event to list activity");
                MainBus.getInstance().post(new ConfClickRxEvent(dataset.get(position).getName(),
                        dataset.get(position).getId()  ));
            }
        });
    }

    @Override
    public int getItemCount() {
        return dataset.size();
    }
}
