package com.audeldugarte.crossoverdevtest.activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;

import com.audeldugarte.crossoverdevtest.R;
import com.audeldugarte.crossoverdevtest.adapters.MedConferenceAdapter;
import com.audeldugarte.crossoverdevtest.internalcomms.ConfClickRxEvent;
import com.audeldugarte.crossoverdevtest.internalcomms.MainBus;
import com.audeldugarte.crossoverdevtest.models.MedConference;

import java.util.ArrayList;
import java.util.List;

import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class ConferencesListActivity extends AppCompatActivity {

    private final String TAG = "ConfsListActivity";

    RecyclerView recyclerViewListConferences;
    List<MedConference> medConfsDataList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_conferences_list);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        /*FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });*/

        initViews();
    }

    private void initViews(){
        recyclerViewListConferences = (RecyclerView) findViewById(R.id.recyclerViewConferencesList);
        medConfsDataList = new ArrayList<MedConference>();
    }

    private void loadDataToLists(){

        if(medConfsDataList != null){
            medConfsDataList.clear();
        }

        medConfsDataList = MedConference.listAll(MedConference.class);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerViewListConferences.setLayoutManager(mLayoutManager);

        MedConferenceAdapter mAdapter = new MedConferenceAdapter(getApplicationContext(), medConfsDataList);
        recyclerViewListConferences.setAdapter(mAdapter);
    }

    /************************************
    *****EVENT BUS W/RX JAVA CODE********
    *************************************/

    @Override
    protected void onResume() {
        super.onResume();
        // Subscribe to EventBus
        MainBus.getInstance().getBusObservable()
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(mainBusSubscriber);

        //PUT THE DATA LOADING IN HERE JUST TO TEST
        loadDataToLists();

    }

    private Subscriber<? super Object> mainBusSubscriber = new Subscriber<Object>() {
        @Override
        public void onCompleted() {
        }

        @Override
        public void onError(Throwable e) {
        }

        @Override
        public void onNext(Object o) {
            if (o instanceof ConfClickRxEvent) {
                ConfClickRxEvent event = (ConfClickRxEvent) o;
                Log.d(TAG, "received click from " + event.getName());
                Intent i = new Intent(getApplicationContext(), MedConfDetailActivity.class);
                Bundle extras = new Bundle();
                extras.putLong("conference_id", event.getConfId());
                i.putExtras(extras);
                startActivity(i);
            }
        }
    };

    @Override
    protected void onPause() {
        super.onPause();
        // Unsubscribe from EventBus
        mainBusSubscriber.unsubscribe();
    }

}
