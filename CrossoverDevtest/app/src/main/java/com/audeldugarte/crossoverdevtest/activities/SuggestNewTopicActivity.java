package com.audeldugarte.crossoverdevtest.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.audeldugarte.crossoverdevtest.R;
import com.audeldugarte.crossoverdevtest.models.SuggestedTopic;

public class SuggestNewTopicActivity extends AppCompatActivity {

    private EditText textSuggestedNewTopicname;
    private EditText textSuggestedNewTopicDescripton;
    private Button btnSaveSuggestedNewTopic;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_suggest_new_topic);

        initViews();
    }

    private void initViews(){
        textSuggestedNewTopicname = (EditText) findViewById(R.id.editTextSuggestingNewTopicName);
        textSuggestedNewTopicDescripton = (EditText) findViewById(R.id.editTextSuggestingNewTopicDescription);

        btnSaveSuggestedNewTopic = (Button) findViewById(R.id.buttonSuggestingNewTopicSave);

        btnSaveSuggestedNewTopic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(arefieldsOk()){
                    Log.d("SUGGESTTOPIC", "saving suggested topic");
                    SuggestedTopic sug = new SuggestedTopic(textSuggestedNewTopicname.getText().toString(),
                            textSuggestedNewTopicDescripton.getText().toString());
                    sug.save();
                    finish();
                }else{
                    Toast.makeText(getApplicationContext(), "one or more fields are empty", Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    private Boolean arefieldsOk(){
        Boolean are = false;

        if(!textSuggestedNewTopicname.getText().toString().isEmpty() &&
                !textSuggestedNewTopicDescripton.getText().toString().isEmpty()){
            are = true;
        }

        return are;
    }
}
