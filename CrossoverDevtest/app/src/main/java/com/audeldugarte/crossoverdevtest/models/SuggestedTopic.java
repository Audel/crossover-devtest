package com.audeldugarte.crossoverdevtest.models;

import com.orm.SugarRecord;

/**
 * Created by audel on 9/5/16.
 */
public class SuggestedTopic extends SugarRecord{

    private String name;
    private String description;

    public SuggestedTopic() {}

    public SuggestedTopic(String name, String description) {
        this.name = name;
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
