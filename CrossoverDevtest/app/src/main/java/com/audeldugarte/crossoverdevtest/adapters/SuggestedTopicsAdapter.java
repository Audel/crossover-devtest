package com.audeldugarte.crossoverdevtest.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.audeldugarte.crossoverdevtest.R;
import com.audeldugarte.crossoverdevtest.internalcomms.ConfClickRxEvent;
import com.audeldugarte.crossoverdevtest.internalcomms.MainBus;
import com.audeldugarte.crossoverdevtest.internalcomms.TopicClickEvent;
import com.audeldugarte.crossoverdevtest.models.SuggestedTopic;

import java.util.List;

/**
 * Created by audel on 9/5/16.
 */
public class SuggestedTopicsAdapter extends RecyclerView.Adapter<SuggestedTopicsAdapter.ViewHolder>{

    private Context context;
    private List<SuggestedTopic> dataset;

    public SuggestedTopicsAdapter(Context context, List<SuggestedTopic> dataset){
        this.context = context;
        this.dataset = dataset;
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        private TextView topicName;
        private TextView topicDescription;

        public ViewHolder(View itemView) {
            super(itemView);

            topicName = (TextView) itemView.findViewById(R.id.textSuggestedTopicName);
            topicDescription = (TextView) itemView.findViewById(R.id.textSuggestedTopicDescription);
        }

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_suggested_topic, parent, false);

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        //TODO make the stuff happen here

        holder.topicDescription.setText(dataset.get(position).getDescription() );
        holder.topicName.setText(dataset.get(position).getName() );

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("FragmentDialog", "about to send the event to list activity");
                MainBus.getInstance().post(new TopicClickEvent( dataset.get(position) ));
            }
        });
    }

    @Override
    public int getItemCount() {
        return dataset.size();
    }
}
