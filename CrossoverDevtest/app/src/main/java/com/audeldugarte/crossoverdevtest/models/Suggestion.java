package com.audeldugarte.crossoverdevtest.models;

/**
 * Created by audel on 9/4/16.
 */
public class Suggestion {
    private String name;
    private String description;
    private Long suggesterId;
    private String suggesterEmail;

    public Suggestion() {}

    public Suggestion(String name, String description, Long suggesterId, String suggesterEmail) {
        this.name = name;
        this.description = description;
        this.suggesterId = suggesterId;
        this.suggesterEmail = suggesterEmail;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getSuggesterId() {
        return suggesterId;
    }

    public void setSuggesterId(Long suggesterId) {
        this.suggesterId = suggesterId;
    }

    public String getSuggesterEmail() {
        return suggesterEmail;
    }

    public void setSuggesterEmail(String suggesterEmail) {
        this.suggesterEmail = suggesterEmail;
    }
}
