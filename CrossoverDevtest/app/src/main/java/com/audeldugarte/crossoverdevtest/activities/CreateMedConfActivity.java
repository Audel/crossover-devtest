package com.audeldugarte.crossoverdevtest.activities;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.audeldugarte.crossoverdevtest.R;
import com.audeldugarte.crossoverdevtest.fragments.DatePickerFragment;
import com.audeldugarte.crossoverdevtest.internalcomms.DatePickedEvent;
import com.audeldugarte.crossoverdevtest.internalcomms.MainBus;
import com.audeldugarte.crossoverdevtest.internalcomms.TopicClickEvent;
import com.audeldugarte.crossoverdevtest.models.ConferenceTopic;
import com.audeldugarte.crossoverdevtest.models.MedConference;

import java.util.ArrayList;
import java.util.List;

import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class CreateMedConfActivity extends AppCompatActivity {

    final static String TAG = "CreateMedConfActivity";

    private EditText textNewConfName;
    private EditText textNewConfLocation;
    private TextView textNewConfStartDate;
    private TextView textNewConfEndDate;
    private Spinner spinnerNewConfTopic;
    private Button buttonSaveNewConf;
    private Boolean startDateSet, endDateSet;
    private List<ConferenceTopic> topicsList;

    private TextView selectedTextEdit;
    private Long idConferenceInEditMode;
    private Boolean isEditingConf;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_med_conf);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        /*FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });*/
        
        isEditingConf = false;
        Bundle extras = getIntent().getExtras();
        if(extras != null){
            if (extras.containsKey("id_conf_edit")){
                idConferenceInEditMode = extras.getLong("id_conf_edit", 0);
                isEditingConf = true;
            }
        }

        initViews();
    }

    private void initViews(){
        textNewConfName = (EditText) findViewById(R.id.editTextNewConfName);
        textNewConfLocation = (EditText) findViewById(R.id.editTextNewConfLocation);
        textNewConfStartDate = (TextView) findViewById(R.id.textNewConfStartDate);
        textNewConfEndDate = (TextView) findViewById(R.id.textNewConfEndDate);
        spinnerNewConfTopic = (Spinner) findViewById(R.id.spinnerNewConfTopic);
        buttonSaveNewConf = (Button) findViewById(R.id.buttonSaveNewConference);

        startDateSet = false;
        endDateSet = false;

        topicsList = ConferenceTopic.listAll(ConferenceTopic.class);
        List<String> topicsNames = new ArrayList<String>();
        topicsNames.add("Topics");
        for(ConferenceTopic mtop : topicsList){
            topicsNames.add(mtop.getName());
        }

        // Creating adapter for spinner
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, topicsNames);

        // Drop down layout style - list view with radio button
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // attaching data adapter to spinner
        spinnerNewConfTopic.setAdapter(dataAdapter);

        buttonSaveNewConf.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (fieldsAreOk()){
                    MedConference mMedConf = new MedConference(textNewConfName.getText().toString(),
                            textNewConfLocation.getText().toString(), textNewConfStartDate.getText().toString(),
                            textNewConfEndDate.getText().toString(),
                            topicsList.get(spinnerNewConfTopic.getSelectedItemPosition()-1).getId()  );
                    //if we are editing the conference then we set the id so it gets updated
                    if(isEditingConf){
                        mMedConf.setId(idConferenceInEditMode);
                    }
                    mMedConf.save();
                }
            }
        });

        textNewConfStartDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectedTextEdit = textNewConfStartDate;
                DialogFragment nDateFragment =  new DatePickerFragment();
                nDateFragment.show(getSupportFragmentManager(), "datePicker");
            }
        });
        textNewConfEndDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectedTextEdit = textNewConfEndDate;
                DialogFragment nDateFragment =  new DatePickerFragment();
                nDateFragment.show(getSupportFragmentManager(), "datePicker");
            }
        });
        
        //AND THE LAST STEP IS LOAD IN CASE CONFERENCE IS BEING EDITED
        if(isEditingConf){
            MedConference mConf = MedConference.findById(MedConference.class, idConferenceInEditMode);
            textNewConfName.setText(mConf.getName());
            textNewConfLocation.setText(mConf.getLocation());
            textNewConfStartDate.setText(mConf.getStartDate());
            textNewConfEndDate.setText(mConf.getEndDate());

            int contTopics = 0;
            for(int i=0;i<topicsList.size();i++){
                if(topicsList.get(i).getId() == mConf.getConferenceTopic()){
                    contTopics = i+1;
                }
            }
            spinnerNewConfTopic.setSelection(contTopics);
        }
    }

    private Boolean fieldsAreOk(){
        Boolean areThey = false;
        if(!textNewConfEndDate.getText().toString().isEmpty() &&
                !textNewConfLocation.getText().toString().isEmpty() &&
                startDateSet && endDateSet && (spinnerNewConfTopic.getSelectedItemPosition() != 0) ){
            areThey = true;
        }

        return areThey;
    }

    /************************************
     *****EVENT BUS W/RX JAVA CODE********
     *************************************/

    @Override
    protected void onResume() {
        super.onResume();
        // Subscribe to EventBus
        MainBus.getInstance().getBusObservable()
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(mainBusSubscriber);

    }

    private Subscriber<? super Object> mainBusSubscriber = new Subscriber<Object>() {
        @Override
        public void onCompleted() {
        }

        @Override
        public void onError(Throwable e) {
        }

        @Override
        public void onNext(Object o) {
            if (o instanceof DatePickedEvent) {
                DatePickedEvent event = (DatePickedEvent) o;
                Log.d(TAG, "received click from " + event.getStringDate());
                selectedTextEdit.setText(event.getStringDate());
                if(selectedTextEdit.getId() == R.id.textNewConfStartDate){
                    startDateSet = true;
                }else {
                    endDateSet = true;
                }
            }
        }
    };

    @Override
    protected void onPause() {
        super.onPause();
        // Unsubscribe from EventBus
        mainBusSubscriber.unsubscribe();
    }

}
