package com.audeldugarte.crossoverdevtest.models;

import com.orm.SugarRecord;

/**
 * Created by audel on 9/4/16.
 */
public class MedConference extends SugarRecord{

    private String name;
    private String location;
    private String startDate;
    private String endDate;
    private Long conferenceTopic;

    public MedConference() {}

    public MedConference(String name, String location, String startDate, String endDate, Long conferenceTopic) {
        this.name = name;
        this.location = location;
        this.startDate = startDate;
        this.endDate = endDate;
        this.conferenceTopic = conferenceTopic;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public Long getConferenceTopic() {
        return conferenceTopic;
    }

    public void setConferenceTopic(Long conferenceTopic) {
        this.conferenceTopic = conferenceTopic;
    }
}
