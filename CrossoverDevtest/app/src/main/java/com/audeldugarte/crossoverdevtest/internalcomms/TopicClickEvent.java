package com.audeldugarte.crossoverdevtest.internalcomms;

import com.audeldugarte.crossoverdevtest.models.SuggestedTopic;

/**
 * Created by audel on 9/5/16.
 */
public class TopicClickEvent {

    private SuggestedTopic sugTopic;

    public TopicClickEvent() {}

    public TopicClickEvent(SuggestedTopic sugTopic) {
        this.sugTopic = sugTopic;
    }

    public SuggestedTopic getSugTopic() {
        return sugTopic;
    }

    public void setSugTopic(SuggestedTopic sugTopic) {
        this.sugTopic = sugTopic;
    }
}
