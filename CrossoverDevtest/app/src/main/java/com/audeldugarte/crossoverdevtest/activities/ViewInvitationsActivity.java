package com.audeldugarte.crossoverdevtest.activities;

import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import com.audeldugarte.crossoverdevtest.R;
import com.audeldugarte.crossoverdevtest.adapters.InvitationsAdapter;
import com.audeldugarte.crossoverdevtest.adapters.MedConferenceAdapter;
import com.audeldugarte.crossoverdevtest.internalcomms.ConfClickRxEvent;
import com.audeldugarte.crossoverdevtest.internalcomms.MainBus;
import com.audeldugarte.crossoverdevtest.internalcomms.TopicClickEvent;
import com.audeldugarte.crossoverdevtest.models.ConfInvitation;

import java.util.List;

import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class ViewInvitationsActivity extends AppCompatActivity {

    final static String TAG = "ViewInvitationsActivity";

    private RecyclerView recyclerViewInvitationsRecv;
    private List<ConfInvitation> invitationsList;
    private Long loggeduserType;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_invitations);

        initViews();
    }

    private void initViews(){
        recyclerViewInvitationsRecv = (RecyclerView) findViewById(R.id.recyclerViewInvitations);

        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        loggeduserType = settings.getLong("usertype", 0);

        loadDataToRecyclerView();
    }

    private void loadDataToRecyclerView(){
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        String loggedUserId = Long.toString( settings.getLong("userid", 0) );

        invitationsList = ConfInvitation.find(ConfInvitation.class, "invited_id = ?", loggedUserId);
        Log.d(TAG, "the query returned " + invitationsList.size() + " entries");

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerViewInvitationsRecv.setLayoutManager(mLayoutManager);

        InvitationsAdapter mAdapter = new InvitationsAdapter(getApplicationContext(), invitationsList);
        recyclerViewInvitationsRecv.setAdapter(mAdapter);
    }

    /************************************
     *****EVENT BUS W/RX JAVA CODE********
     *************************************/

    @Override
    protected void onResume() {
        super.onResume();
        // Subscribe to EventBus
        MainBus.getInstance().getBusObservable()
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(mainBusSubscriber);

    }

    private Subscriber<? super Object> mainBusSubscriber = new Subscriber<Object>() {
        @Override
        public void onCompleted() {
        }

        @Override
        public void onError(Throwable e) {
        }

        @Override
        public void onNext(Object o) {
            if (o instanceof ConfClickRxEvent) {
                ConfClickRxEvent event = (ConfClickRxEvent) o;
                Log.d(TAG, "received click from " + event.getName());
                loadDataToRecyclerView();
            }
        }
    };

    @Override
    protected void onPause() {
        super.onPause();
        // Unsubscribe from EventBus
        mainBusSubscriber.unsubscribe();
    }
}
