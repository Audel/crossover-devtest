package com.audeldugarte.crossoverdevtest.activities;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.audeldugarte.crossoverdevtest.R;
import com.audeldugarte.crossoverdevtest.models.ConferenceTopic;

import java.util.ArrayList;
import java.util.List;

public class TopicsViewActivity extends AppCompatActivity {

    private Spinner spinnerOldTopics;
    private EditText textNewTopicName;
    private EditText textNewTopicDescription;
    private Button btnSaveNewTopic;

    private Button btnDeleteShownTopic;
    private Button btnEditShownTopic;
    private Boolean editionActive;
    private Long editingId;

    private List<ConferenceTopic> topicsList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_topics_view);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        /*FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });*/

        initViews();
    }

    private void initViews(){

        spinnerOldTopics = (Spinner) findViewById(R.id.spinnerTopicsCreated);
        textNewTopicName = (EditText) findViewById(R.id.editTextNewTopicName);
        textNewTopicDescription = (EditText) findViewById(R.id.edittextNewTopicDescription);
        btnSaveNewTopic = (Button) findViewById(R.id.buttonSaveNewTopic);

        btnEditShownTopic = (Button) findViewById(R.id.buttonEditTopic);
        btnDeleteShownTopic = (Button) findViewById(R.id.buttonDeleteTopic);

        btnSaveNewTopic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(fieldsOk()){
                    ConferenceTopic daTopic = new ConferenceTopic(textNewTopicName.getText().toString(),
                            textNewTopicDescription.getText().toString());
                    if(editionActive){
                        daTopic.setId(editingId);
                        editionActive =false;
                    }
                    daTopic.save();
                    textNewTopicName.getText().clear();
                    textNewTopicDescription.getText().clear();
                    reloadSpinner();
                }
            }
        });

        editionActive = false;
        editingId = 0l;

        btnEditShownTopic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Find selected topic on spinner and delete it using its Id
                if(spinnerOldTopics.getSelectedItemPosition() > 0){
                    editionActive = true;
                    editingId = topicsList.get(spinnerOldTopics.getSelectedItemPosition() -1).getId();
                    textNewTopicName.setText(topicsList.get(spinnerOldTopics.getSelectedItemPosition() -1).getName());
                    textNewTopicDescription.setText(topicsList.get(spinnerOldTopics.getSelectedItemPosition() -1).getDescription());
                }
            }
        });
        btnDeleteShownTopic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //delete selected topic according to item selected on spinner
                if(spinnerOldTopics.getSelectedItemPosition() > 0) {
                    Long idTODel = topicsList.get(spinnerOldTopics.getSelectedItemPosition() - 1).getId();
                    ConferenceTopic delTop = ConferenceTopic.findById(ConferenceTopic.class, idTODel);
                    delTop.delete();
                    reloadSpinner();
                }
            }
        });

        reloadSpinner();

    }

    private Boolean fieldsOk(){
        Boolean isit = false;

        if(!textNewTopicName.getText().toString().isEmpty() &&
                !textNewTopicDescription.getText().toString().isEmpty()){
            isit = true;
        }

        return isit;
    }

    private void reloadSpinner(){
        topicsList = ConferenceTopic.listAll(ConferenceTopic.class);
        List<String> topicsNames = new ArrayList<String>();
        topicsNames.add("Existing topics");
        for(ConferenceTopic mtop : topicsList){
            topicsNames.add(mtop.getName());
        }

        // Creating adapter for spinner
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, topicsNames);

        // Drop down layout style - list view with radio button
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // attaching data adapter to spinner
        spinnerOldTopics.setAdapter(dataAdapter);
    }

}
