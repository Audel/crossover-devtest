package com.audeldugarte.crossoverdevtest.fragments;

import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.audeldugarte.crossoverdevtest.R;
import com.audeldugarte.crossoverdevtest.models.User;
import com.audeldugarte.crossoverdevtest.utils.UtilFunctions;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by audel on 9/4/16.
 */
public class RegisterDialogFragment extends DialogFragment {

    private EditText textUsrLogin;
    private EditText textEmail;
    private EditText textPassword;
    private EditText textUserFirstName;
    private EditText textUserLastName;
    private EditText textConfirmPassword;
    private Button btnGoRegister;
    private Button btnCancelRegister;

    public RegisterDialogFragment() {
        // Empty constructor required for DialogFragment
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_dialog_register, container);

        getDialog().setTitle(getResources().getString(R.string.tittle_user_register));
        getDialog().setCanceledOnTouchOutside(false);

        initViews(view);

        return view;
    }

    private void initViews(View mainView){
        //textUsrLogin = (EditText) mainView.findViewById(R.id.textRegisterUserLogin);
        textEmail = (EditText) mainView.findViewById(R.id.textRegisterEmail);
        textPassword = (EditText) mainView.findViewById(R.id.textRegisterPassword);
        textUserFirstName = (EditText) mainView.findViewById(R.id.textRegisterUserFirstName);
        textUserLastName = (EditText) mainView.findViewById(R.id.textRegisterUserLastName);
        textConfirmPassword = (EditText) mainView.findViewById(R.id.textRegisterPasswordConfirm);
        btnGoRegister = (Button) mainView.findViewById(R.id.buttonDialogRegisterUser);
        btnCancelRegister = (Button) mainView.findViewById(R.id.buttonDialogCancelRegister);

        btnGoRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(UtilFunctions.checkValidEmail(textEmail.getText().toString()) ){
                    String emailDomain = textEmail.getText().toString().split("@")[1];

                    if(!textUserFirstName.getText().toString().isEmpty() &&
                            !textUserLastName.getText().toString().isEmpty() &&
                            !textEmail.getText().toString().isEmpty() &&
                                /*!textUsrLogin.getText().toString().isEmpty() &&*/
                            !textPassword.getText().toString().isEmpty() &&
                            !textConfirmPassword.getText().toString().isEmpty()){
                        registerUser();
                    }else{
                        Toast.makeText(getActivity(), getResources().getString(R.string.fields_empty), Toast.LENGTH_LONG).show();
                    }

                }else{
                    Toast.makeText(getActivity(), getResources().getString(R.string.invalid_email), Toast.LENGTH_LONG).show();
                }

                /*Log.d("FragmentDialog", "about to send the event i think");
                MainBus.getInstance().post(new MiCommEvent("probando evento de prueba en Rx"));
                dismiss();*/
            }
        });

        btnCancelRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

    }

    private void registerUser(){
        User theNewUser = new User(textUserFirstName.getText().toString() + " " + textUserLastName.getText().toString(),
                textEmail.getText().toString(), UtilFunctions.calculateMd5(textPassword.getText().toString()), 1l);

        theNewUser.save();

        Toast.makeText(getActivity(), getResources().getString(R.string.register_success), Toast.LENGTH_LONG).show();

        dismiss();
    }

}
